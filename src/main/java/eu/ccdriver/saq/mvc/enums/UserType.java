package eu.ccdriver.saq.mvc.enums;

public enum UserType {
    USER, CREATOR;
}
