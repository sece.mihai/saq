package eu.ccdriver.saq.mvc.enums;

public enum UserState {
    NEW, ACTIVE, RE_ACTIVE, DEACTIVATED;
}
