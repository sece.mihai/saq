package eu.ccdriver.saq.mvc.commands;

import eu.ccdriver.saq.mvc.enums.UserState;
import eu.ccdriver.saq.mvc.enums.UserType;
import lombok.Getter;
import lombok.Setter;
import net.bytebuddy.utility.RandomString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UserCommand {
    @NotNull
    @NotBlank
    private String userName;
    private String password = RandomString.make();
    @NotNull
    @NotBlank
    private String firstName;
    @NotNull
    @NotBlank
    private String lastName;
    @NotNull
    @NotBlank
    private String organisation;
    @NotNull
    @NotBlank
    private String jobTitle;
    @NotNull
    private UserType userType;
    private UserState userState = UserState.NEW;
}
