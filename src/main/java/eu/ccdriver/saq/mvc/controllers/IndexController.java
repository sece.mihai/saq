package eu.ccdriver.saq.mvc.controllers;

import eu.ccdriver.saq.mvc.commands.UserCommand;
import eu.ccdriver.saq.services.IndexService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
public class IndexController {
    private final IndexService indexService;

    public IndexController(IndexService indexService) {
        this.indexService = indexService;
    }

    @GetMapping
    @RequestMapping({"/", "/index"})
    public String accountCreation() {
        return TemplateNames.INDEX_TEMPLATE;
    }

    @GetMapping
    @RequestMapping("/open-create-user")
    public String openAddUser(Model model) {
        model.addAttribute("userCommand", new UserCommand());
        return TemplateNames.ADD_USER;
    }

    @PostMapping
    @RequestMapping("/create-user")
    public String saveUser(@Valid @ModelAttribute UserCommand userCommand, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return TemplateNames.ADD_USER;
        }
        this.indexService.createUser(userCommand);
        return "redirect:/index";
    }
}
