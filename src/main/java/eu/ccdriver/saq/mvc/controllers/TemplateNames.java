package eu.ccdriver.saq.mvc.controllers;

public interface TemplateNames {
    String INDEX_TEMPLATE = "index";
    String ADD_USER = "add-user";
}
