package eu.ccdriver.saq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("eu.ccdriver")
public class SAQApplication {
    public static void main(String[] args) {
        SpringApplication.run(SAQApplication.class, args);
    }
}
