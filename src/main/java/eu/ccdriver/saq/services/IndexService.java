package eu.ccdriver.saq.services;

import eu.ccdriver.saq.mvc.commands.UserCommand;

public interface IndexService {
    void createUser(UserCommand userCommand);
}
