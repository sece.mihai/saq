package eu.ccdriver.saq.services;

import eu.ccdriver.saq.mvc.commands.UserCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class IndexServiceImpl implements IndexService {
    private final List<UserCommand> users = new ArrayList<>();

    @Override
    public void createUser(UserCommand userCommand) {
        this.users.add(userCommand);
        log.info("New user created");
    }
}
